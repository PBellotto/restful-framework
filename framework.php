<?php
    require_once 'client.php'; //Requere o documento client.php apenas uma vez
    use \Conn\Connection as Connection; //Usa a classe Connection dentro da namespace Conn como Connection
    use \Client\Client as Client; //Usa a classe Client dentro da namespace Client como Client 

    $client = new Client; //Instancia uma classe Client

    $client->setHeader('Content-Type', 'application/json'); //Envia um cabeçalho para a resposta

    $client->ifGet('helloworld', 'helloWorld'); //Define o uri e a função a ser executada
    $client->ifGet('getclients', 'getClients'); //Define o uri e a função a ser executada

    function helloWorld() //Função helloWorld
    {
        echo json_encode('Hello world'); //Retorna um JSON com a resposta
    }
    function getClients() //Função getClients
    {
        $conn = new Connection; //Instancia uma nova conexão
        $conn->getAttributes('yourHost', 'yourDB', 'youUser', 'yourPassword'); //Passa os atributos da conexão 
        $result = $conn->executeSimpleQuery('SELECT * FROM `your_table`'); //Passa uma query simples por parâmetro
        $result->execute(); //Executa a query
        $fetch = $result->fetchAll(); //Reúne todos as linhas retornadas em um único objeto
        echo json_encode($fetch, JSON_PRETTY_PRINT); //Retorna um JSON com a resposta
    }
?>