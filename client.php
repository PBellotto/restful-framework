<?php
    namespace Conn
    {
        use \PDO;

        interface iConnection 
        {
            public function getAttributes($http, $db, $user, $password); 
            public function prepareQuery($sql);
            public function executeSimpleQuery($sql);
            public function disconnectPDO();
        }
        class Connection implements iConnection  
        {
            private $instance; //Atributo instance do tipo privado
            private $http; //Atributo http do tipo público
            private $db; //Atributo db do tipo público
            private $user; //Atributo user do tipo público
            private $password; //Atributo password do tipo público 

            private function connectPDO() //Método de conexão no BD
            {
                try
                {
                    $this->instance = new PDO('mysql:host='.$this->http.';dbname='.$this->db, $this->user, $this->password, array(PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES `utf8`")); //Instacia classe PDO e passa os parâmetros de conexão, iguala ao atributo instance
                    $this->instance->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION); //Seta atributos de conexão 
                    $this->instance->setAttribute(PDO::ATTR_DEFAULT_FETCH_MODE, PDO::FETCH_OBJ); //Seta atributos de conexão

                    return $this->instance; //Retorna a classe PDO instaciada no atributo instance da classe Conexao e com atributos internos setados
                }
                catch(PDOException $state) //Pega exceção, caso haja
                {
                    return 'Erro ao conectar ao BD: '.$state->getMessage(); //Retorna o erro, caso exista
                }
            }

            public function getAttributes($http, $db, $user, $password) //Método público para requerer atributos de conexão
            {
                $this->http = trim($http); //Atribui o valor tratado da variável http para o atributo interno http
                $this->db = trim($db); //Atribui o valor tratado da variável db para o atributo interno db
                $this->user = trim($user); //Atribui o valor tratado da variável user para o atributo interno user
                $this->password = trim($password); //Atribui o valor tratado da variável password para o atributo interno password
            }

            public function prepareQuery($sql) //Método para preparar a query
            {
                return $this->connectPDO()->prepare($sql); //Retorna a query passada por parâmetro preparada
            }

            public function executeSimpleQuery($sql) //Método para executar uma query simples, sem prepará-la
            {
                return $this->connectPDO()->query($sql); //Retorna a execução da query no servidor
            }

            public function disconnectPDO() //Método pra desconectar do servidor
            {
                $this->instance = null; //Seta o atributo instance como nulo
                $this->http = null; //Seta o atributo http como nulo
                $this->db = null; //Seta o atributo db como nulo
                $this->user = null; //Seta o atributo user como nulo
                $this->password = null; //Seta o atributo password como nulo
            }
        }
    }

    namespace Client
    {
        interface iClient
        {
            public function trimParam($param);
            public function setHeader($contentType, $type);
            public function ifGet($keyword, $function); 
        }
        class Client implements iClient
        {  
            private $method; //Define o atributo method
            private $uri; //Define o atributo uri
            private $uriExploded = array(); //Define o atributo uriExploded como uma array
            private $queryString; //Define o atributo queryString
            private $authUser; //Define o atributo authUser
            private $functionRequired; //Define o atributo functionRequired
            private $keywordRequired; //Define o atributo keywordRequired
            private $search; //Define o atributo pesquisa

            private function getMethod() //Cria o método getMethod
            {
                $this->method = $_SERVER['REQUEST_METHOD']; //Atribui o método utilizado na requisição http para o atributo interno method
                return $this->method; //Retorna o atributo method
            }

            private function getUri() //Cria o método para aquisição de Uri 
            {
                $this->uri = $_SERVER['REQUEST_URI']; //Atribui a uri requerida para o atributo interno uri
                return $this->uri; //Retorna o atributo uri
            }

            private function matchStr() //Cria o método para verificar se a string passada por parâmetro na função ifGet é igual a que existe na uri
            {
                $match = strcmp(basename($_SERVER['PHP_SELF']), $this->keywordRequired); //Compara binariamente a string passada com a string existente na uri
                if($match == 0) //Se o resultado for igual a 0
                {
                    return true; //Retorna verdadeiro
                }
                else //Caso contrário
                {
                    return false; //Retorna falso
                }
            }

            /*private function getUriExploded() //Retorna uma array da uri
            {
                $this->uriExploded = explode('/', $_SERVER['REQUEST_URI']);
                return $this->uriExploded;
            }*/

            private function getQuery()
            {
                if(isset($_SERVER['QUERY_STRING']))
                {
                    $query = $_SERVER['QUERY_STRING'];
                    parse_str($query); 
                    if(isset($q))
                    {
                        return $q; //sends a parsed variable with uri's query as a return
                    }
                    else
                    {
                        return null;
                    }
                }
                else
                {
                    return null;
                }
            }

            private function getAuthUser() //Cria o método para autenticação de usuário
            {
                $this->authUser = $_SERVER['PHP_AUTH_USER']; //Obtém o usuário e atribui para o atributo authUser
                return $this->authUser; //Retorna o atributo authUser
            }

            public function trimParam($param) //Cria o método para aparar os lados de uma string qualquer
            {
                return trim($param); //Retorna a string aparada
            }

            /*private function lastIndex()
            {
                $uri_exploded = array();
                $uri_exploded = $this->getUri();
                end($uri_exploded);
                $uri_last_key = key($uri_exploded);
                return $uri_last_key; //returns the last index of the uri's array
            }*/
            
            private function funcExists() //Cria o método para verificar se a função digitada pelo usuário realmente existe
            {
                $funcExists = function_exists($this->functionRequired); //Atribui o valor verdadeiro ou false para funcExists conforme o resultado da função function_exists
                return $funcExists; //retorn a variável com a resposta
            }

            public function setHeader($contentType, $type)
            {
                return header($contentType.': '.$type);
            }

            public function ifGet($keyword, $function) //Cria o método para verificar o uri e executar a função desejada
            {
                $this->keywordRequired = $keyword; //Atribui o valor de keyword para o atributo interno keywordRequired
                $this->functionRequired = $function; //Atribui o valor de function para o atributo interno functionRequired
                if($this->getMethod() == 'GET' && $this->getQuery() === null)  //Verifica se o método usado é igual a get e se existe uma query na uri
                {
                    if($this->funcExists() === true) //Verifica se a função passada por parâmetro existe
                    {
                        if($this->matchStr() === true) //Verifica se a palavra-chave passada por parâmetro existe na uri
                        {
                            return call_user_func($this->functionRequired); //Retorna a função requerida pelo usuário
                        }
                    }
                }
            }
        }
    }
?>